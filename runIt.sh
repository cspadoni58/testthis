#!/bin/bash

# Pretty simple scripts to run the aip and mf binaries

echo "--------------------------------------------------------"
echo " "
echo "------------------ Start of Processing -----------------"
echo " " 

# First, run the aip
./aip/aipMain

# And then the mf
./mf/mfMain

echo ""
echo "------------------- End of Processing ------------------"
echo ""
echo "--------------------------------------------------------"

